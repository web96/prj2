import React, { Component } from 'react';
import MySideItem from './MySideItem'

class MySideUl extends Component {
    render() {
        return (
                <ul className="list-unstyled components" id="sidebarlist">
                    <li className="active">
                        <div id="overview-div" className="sidebar-items">
                            <i id="overview-icon" className="material-icons material-hover">
                                home
                            </i>
                            <a id="overview-link" className="material-hover"><span>Overview</span></a>
                            <a id="setting-link"><i id="setting-icon" className="material-icons">settings</i></a>
                        </div>
                    </li>
                        <MySideItem iconName={"analytics"} spanName={"Analytics"} iconShape={"equalizer"}/>
                    <li>
                        <div className="sidebar-seperator">
                    <span id="develop">
                        DEVELOP
                    </span>
                        </div>
                    </li>
                        <MySideItem iconName={"authentication"} spanName={"Authentication"} iconShape={"people"}/>
                        <MySideItem iconName={"database"} spanName={"Database"} iconShape={"dns"}/>
                        <MySideItem iconName={"storage"} spanName={"Storage"} iconShape={"panorama"}/>
                        <MySideItem iconName={"hosting"} spanName={"Hosting"} iconShape={"language"}/>
                        <MySideItem iconName={"functions"} spanName={"Functions"} iconShape={"settings_ethernet"}/>
                        <MySideItem iconName={"testlab"} spanName={"Test Lab"} iconShape={"settings_ethernet"}/>
                        <MySideItem iconName={"crash"} spanName={"Crash Reporting"} iconShape={"bug_report"}/>
                        <MySideItem iconName={"performance"} spanName={"Performance"} iconShape={"dns"}/>

                    <li>
                        <div className="sidebar-seperator">
                    <span id="grow">
                        GROW
                    </span>
                        </div>
                    </li>

                    <MySideItem iconName={"notifications"} spanName={"Notifications"} iconShape={"chat"}/>
                    <MySideItem iconName={"remote"} spanName={"Remote Config"} iconShape={"dns"}/>
                    <MySideItem iconName={"dynamic"} spanName={"Dynamic Links"} iconShape={"link"}/>


                    <li>
                        <div className="sidebar-seperator">
                    <span id="earn">
                        EARN
                    </span>
                        </div>
                    </li>

                    <MySideItem iconName={"admob"} spanName={"AdMob"} iconShape={"dns"}/>

                </ul>

        );
    }
}

export default MySideUl;