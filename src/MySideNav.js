import React, { Component } from 'react';
import MySideUl from "./MySideUl";
import ToggleButton from "./ToggleButton"



class MySideNav extends Component {
    render() {
        return (
        <nav id="sidebar" className="sidebar">
            <MySideUl/>
            <div id="modifydiv">
                <div id="antimodify">
                <span id="blaze">
                Blaze
                </span>
                    <br>
                    </br>
                    <span id="pay">
                Pay as you go

                </span>
                </div>
                <div >
                <span id="modify">
                    MODIFY
                </span>
                </div>
            </div >
            <ToggleButton/>
        </nav>
    );
}
}

export default MySideNav;