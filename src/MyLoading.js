import React, { Component } from "react";

import {Navbar, Nav, NavItem} from "react-bootstrap";

function MyLoading(props) {
    return (
        <div id="loader" style={{display: ((props.appIsLoaded) ? "none":"initial")}}>
            <Navbar.Brand>
                <img src={require("./img/firebase.png")} id="nav-icon-yellow-loading" />
            </Navbar.Brand>
            <div id="followingBallsG">
                <div id="followingBallsG_1" className="followingBallsG"></div>
                <div id="followingBallsG_2" className="followingBallsG"></div>
                <div id="followingBallsG_3" className="followingBallsG"></div>
                <div id="followingBallsG_4" className="followingBallsG"></div>
            </div>
        </div>
    );
}

export default MyLoading;
