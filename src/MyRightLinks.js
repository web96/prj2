import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

function MyRightLinks(props) {
    return (
        <div id="go-to-docs" className="inline-rights">
            <a href="#">Go to docs</a>
        </div>
    );
}

export default MyRightLinks;
