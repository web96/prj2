import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

function MyPictureHolder(props) {
    return (
        <div id="picture-container" className="inline-rights">
            <img id="usr-img" src={props.imageSource} alt="Daenerys" />
        </div>
    );
}

export default MyPictureHolder;
