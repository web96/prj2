import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

import MyThreeDotsHover from "./MyThreeDotsHover";

class MyThreeDots extends Component {
    constructor(props) {
        super(props);
        this.state = {clicked: false};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){
        this.setState((prevState, props) => ({clicked: !prevState.clicked}))
    }


    render() {
        return (
            <div id="three-dots" className="inline-rights" onClick={this.handleClick}>
                <div id="three-dots-lower-div">
                    <i id="three-dots-icon" className="material-icons">more_vert</i>
                </div>
                <MyThreeDotsHover visibility={(this.state.clicked) ? 'visible':'hidden'}/>
                {/*<MyThreeDotsHover visibility={()=>'hidden'}/>*/}
            </div>
        );
    }
}

export default MyThreeDots;
