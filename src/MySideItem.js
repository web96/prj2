import React, { Component } from 'react';


class MySideItem extends Component {
    render() {
        return (


            <li>
                <div id={this.props.iconName +"-div"} className="sidebar-items">
                     <a id={this.props.iconName+"-link"}>
                        <i id={this.props.iconName+"-icon"} className="material-icons">
                            {this.props.iconShape}
                          </i>
                     <span>{this.props.spanName}</span>
                         </a>
             </div>
                    </li>

        );
    }
}

export default MySideItem;