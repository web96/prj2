import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

import MyNavbarHeader from "./MyNavbarHeader";

import "./style.css";
import MyNavInlineRight from "./MyNavInlineRight";

function toggleWrapper() {
    var myButtonClasses = document.getElementById("wrapper").classList;

    if (myButtonClasses.contains("open")) {
        myButtonClasses.remove("open");
    } else {
        myButtonClasses.add("open");
    }

}

class MyNav extends Component {
    render() {
        return (
            <Nav className="navbar" id="main-nav" pullRight>
                <MyNavbarHeader toggleFunction={toggleWrapper} />
                <MyNavInlineRight/>
            </Nav>
        );
    }
}

export default MyNav;
