import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";


class MyThreeDotsHover extends Component {
    render() {
        return (
            <div id="three-dots-hover" style={{visibility: this.props.visibility}}>
                <div className="three-dots-lower-div">
                    <span>Firebase docs<i className="material-icons">launch</i></span>
                </div>
                <div className="three-dots-lower-div">
                    <span>Alert subscriptions</span>
                </div>
            </div>
        );
    }
}

export default MyThreeDotsHover;
