import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";


function MyNavbarLogo(props) {
    return (
        <Navbar.Brand>
            <img src={props.imageSource} id={props.innerId}/>
        </Navbar.Brand>
    );
}

export default MyNavbarLogo;

