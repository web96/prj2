import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

import MyNavbarLogo from "./MyNavbarLogo";

function MyNavbarHeader(props) {
    return (
        <Navbar.Header>
            <Button id="menu-button" onClick={props.toggleFunction}>
                <i className="material-icons">menu</i>
            </Button>
            <MyNavbarLogo imageSource="http://s8.picofile.com/file/8314707350/firebase.png" innerId="nav-icon-yellow"/>
            <MyNavbarLogo imageSource="http://www.gstatic.com/mobilesdk/160323_mobilesdk/images/firebase_logotype_white_18dp.svg" innerId="nav-icon-svg"/>
            <Navbar.Brand><div id="nav-overview">Overview</div></Navbar.Brand>
        </Navbar.Header>
    );
}

export default MyNavbarHeader;

