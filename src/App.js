import React, { Component } from 'react';

import "./loading.css";
import "./App.css";


import {Button} from "react-bootstrap";
import MyLoading from "./MyLoading";
import AllBody from "./AllBody";


class App extends Component {
    constructor (props) {
        super(props);
        this.state = {loaded: false};
    }
    componentDidMount() {
        // setTimeout(function(){
        //         document.getElementById("loader").style.display = "none";
        //         document.getElementById("all-body").style.display = "initial";
        //     },
        //     3000);
        setTimeout(()=>this.setState({loaded: true}), 2000);
    }

    render() {
        return (
            <div>
                <MyLoading appIsLoaded={this.state.loaded} />
                <AllBody appIsLoaded={this.state.loaded} />
            </div>
        );
    }
}

export default App;
