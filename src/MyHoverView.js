import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

function MyHoverView(props) {
    return (
        <div id="visibility-container" className="inline-rights">
            <div id="view-only">
                        <span id="visibility-icon-container">
                            <i id="visibility-icon" className="material-icons">visibility</i>
                        </span>
                <span id="view-only-text-container">View only</span>
            </div>
            <div id="hover-view-only">
                <span>This is a demo project</span>
            </div>
        </div>
    );
}

export default MyHoverView;
