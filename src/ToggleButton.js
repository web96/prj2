import React, { Component } from 'react';

var toggleStyle = {
    textAlign: 'center',
};
function toggleWrapper() {
    var myButtonClasses = document.getElementById("wrapper").classList;

    if (myButtonClasses.contains("toggled")) {
        myButtonClasses.remove("toggled");
    } else {
        myButtonClasses.add("toggled");
    }

}


class ToggleButton extends Component {
    render() {
        return (

            <div style={toggleStyle} >
                <a id="menu-toggle" onClick={toggleWrapper} >
                     <i className="material-icons">chevron_left</i>
                    </a>
            </div>
        );}}

export default ToggleButton;