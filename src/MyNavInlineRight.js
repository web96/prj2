import React, { Component } from "react";

import {Navbar, Nav, NavItem, NavDropdown, MenuItem, Button} from "react-bootstrap";

import MyHoverView from "./MyHoverView";

import "./style.css";
import MyRightLinks from "./MyRightLinks";
import MyPictureHolder from "./MyPictureHolder";
import MyThreeDots from "./MyThreeDots";

class MyNavInlineRight extends Component {
    render() {
        return (
            <Nav pullRight id="inline-rights-container">

                <MyHoverView/>
                <MyRightLinks/>
                <MyThreeDots/>
                {/*<div id="three-dots" className="inline-rights">*/}
                    {/*<div id="three-dots-lower-div">*/}
                        {/*<i id="three-dots-icon" className="material-icons">more_vert</i>*/}
                    {/*</div>*/}
                    {/*<div id="three-dots-hover">*/}
                        {/*<div className="three-dots-lower-div">*/}
                            {/*<span>Firebase docs<i className="material-icons">launch</i></span>*/}
                        {/*</div>*/}
                        {/*<div className="three-dots-lower-div">*/}
                            {/*<span>Alert subscriptions</span>*/}
                        {/*</div>*/}
                    {/*</div>*/}
                {/*</div>*/}
                <MyPictureHolder imageSource="http://s8.picofile.com/file/8314707834/user.jpg"/>
            </Nav>
        );
    }
}

export default MyNavInlineRight;
