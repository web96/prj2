import React, { Component } from 'react';

import MyNav from "./MyNav";
import MySideBar from "./MySideBar";
import MyCenter from "./MyCenter";


class AllBody extends Component {
    render() {
        return (
            <div id="all-body" style={{visibility: ((this.props.appIsLoaded) ? "visible" : "hidden")}}>
                <MyNav id="my-nav"/>
                <MySideBar id="my-side-bar"/>
                <MyCenter id="my-center"/>
            {/*<div id="all-body" style={{display: ((this.props.appIsLoaded) ? "initial" : "none")}}>*/}
            </div>
        );
    }
}

export default AllBody;
