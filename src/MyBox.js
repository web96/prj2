import React, { Component } from 'react';


class MyBox extends Component {
    render() {
        return (
            <div className="box">
                <img className="box-img" src={this.props.sors} />
                <div className="box-text">
                    <div className="box-title">Analytics</div>
                    <div className="box-details">Get detailed analytics to measure and analyze how users engage with your app</div>
                    <div className="box-bottom">
                        <div>
                            <a className="box-learnmore" href="#">Learn more</a>
                            <button className="box-start" type="button">GET STARTED</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MyBox;